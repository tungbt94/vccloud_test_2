content = IO.readlines("INPUT")
# list of line in file
lines = Array.new
# list of words in file
words = Array.new
# list of result
list_result = Array.new
# get all line and word
content.each do |line|
  lines.push(line.to_s.downcase!)
  words_in_line = line.split(" ")
  words_in_line.each do |word|
    words.push(word.gsub(/[.,!;]/,'').downcase)
  end
end

words.sort!

# define struct result
Result = Struct.new(:count, :word, :position)
# array line include each word
position = Array.new
# get all result
while(words.count > 0)
  word = words.first
  count = words.count(word)
  position = Array.new
  for i in 0..lines.size - 1 do
    position.push(i + 1) if lines[i].include?(word)
  end
  result = Result.new(count,word, position)
  list_result.push(result)
  while(index = words.index(word))
    words.delete_at(index)
  end
end

# sort list result
list_result.sort_by! {|result| [-result.count] }

# puts result
list_result.each do |result|
  puts "#{result.count} #{result.word} #{result.position}"
end
