Node = Struct.new(:index, :left, :right)
tree_a = Array.new()
tree_a.push(Node.new(1, 2, 3))
tree_a.push(Node.new(2, 4, 5))
tree_a.push(Node.new(3, 6, 7))
tree_a.push(Node.new(4, 0, 0))
tree_a.push(Node.new(5, 0, 0))
tree_a.push(Node.new(6, 0, 0))
tree_a.push(Node.new(7, 0, 0))

tree_b = Array.new()
tree_b.push(Node.new(3, 6, 7))
tree_b.push(Node.new(6, 0, 0))
tree_b.push(Node.new(7, 0, 0))

puts tree_a
puts tree_b

a_include_b = true
tree_b.each do |node|
  if tree_a.include?(node) == false
    a_include_b = false
    break
  end
end

if a_include_b
  puts "tree_b is a subtree of tree_a"
else
  puts "tree_b is not a subtree of tree_a"
end
